FROM alpine:3.14

RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/community" | tee -a /etc/apk/repositories

RUN apk update

RUN apk upgrade

RUN apk add alpine-sdk build-base apk-tools alpine-conf busybox \
  fakeroot syslinux xorriso squashfs-tools sudo \
  mtools dosfstools grub-efi git

RUN adduser -h /home/build -D build -G abuild

RUN echo '%abuild ALL=(ALL) NOPASSWD: ALL' | tee -a /etc/sudoers

RUN sudo -u build abuild-keygen -i -a -n

RUN git clone --progress --verbose https://git.alpinelinux.org/cgit/aports/ /home/build/aports

WORKDIR /home/build/aports/scripts

RUN apk add dhcpcd
