#!/bin/bash -x
docker build -t "alpineboot" .

# docker run -it --rm -v $PWD:/host alpineboot /bin/sh
# exit 0

OUTPUT_DIR=$PWD/iso

[ -d $OUTPUT_DIR ] || mkdir $OUTPUT_DIR

docker run -it --rm -v $OUTPUT_DIR:/iso -v $PWD/mkimg.live.sh:/home/build/aports/scripts/mkimg.live.sh  \
         alpineboot \
         sudo -u build /bin/sh mkimage.sh \
         --arch x86_64 \
         --tag 3.14 \
         --outdir /iso \
         --repository http://dl-cdn.alpinelinux.org/alpine/v3.14/main \
         --repository http://dl-cdn.alpinelinux.org/alpine/v3.14/community \
         --profile live

