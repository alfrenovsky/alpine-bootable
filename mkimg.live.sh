profile_live() {
	title="Live"
	desc="Alpine live."
	profile_base
	profile_abbrev="live"
	image_ext="iso"
	arch="x86_64"
	output_format="iso"
#	kernel_cmdline="nomodeset"

	kernel_addons=""

	local _k _a
	for _k in $kernel_flavors; do
		apks="$apks linux-$_k"
		for _a in $kernel_addons; do
			apks="$apks $_a-$_k"
		done
	done

	apks="$apks linux-firmware"
  apks=""

  set

}
